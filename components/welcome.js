import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Button
} from 'react-native';


class MainContainer extends Component  {
  constructor(props) {
    super(props);
    this.state = { pageNum: 1};
  }


  changeComponentPage() {
    this.setState((prevState) => {
      let pageNum;
      if(prevState.pageNum === 3){
        pageNum = 1;
      }
      else {
        pageNum = prevState.pageNum
        ++pageNum
      }
      return {pageNum:pageNum}
    })
  }

  render () {
    return (
      <View style= {styles.container}>
        <Text style={styles.welcome}>{this.state.pageNum}</Text>
        <Button
          onPress={() => this.changeComponentPage()}
          title="Click Me"
          color="#841584"
        />
      </View>
    );
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});

export default MainContainer;
